# Đặt hàng Pizza

## 📄 Description

- ### Cho phép thêm pizza vào giỏ hàng, áp mã giảm giá nếu có, điền thông tin thanh toán
![Full page](images/fullPage.png)
## ✨ Feature

- Thêm sản phẩm vào giỏ hàng:
  ![add pizza](images/addPizza.png)

- Trang thông tin giỏ hàng:
  ![cart page](images/CartPage.png)

- Thông tin thanh toán:
  ![payment info](images/paymentInfo.png)

- Thanh toán thành công:
  ![success payment](images/successPayment.png)

- Trang quản lý đơn hàng:
  ![admin page](images/adminPage.png)

- Sửa thông tin đơn hàng:
  ![edit order](images/editOrder.png)

- Xóa đơn hàng:
  ![delete order](images/deleteOrder.png)


## 🧱 Technology

- Front-end:

  - Bootstrap 4
  - DataTable
  - Javascript
  - Jquery
  - Ajax
